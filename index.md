---
layout: cv
title: Robertus Diawan Chris' CV
---
# Robertus Diawan Chris

<div id="webaddress">
<i class="fa fa-home"></i> <a href="http://bruhtus.github.io">bruhtus.github.io</a>
|
<i class="fa fa-github"></i> <a href="http://github.com/bruhtus">bruhtus</a>
|
<i class="fa fa-envelope"></i> <a href="mailto:diawan@pm.me">diawan@pm.me</a>
</div>

## Education

`2014-2020`
__Institut Teknologi Sepuluh Nopember__ <br>
Bachelor of Mathematics. Major in Computer Science.

## Skills

- Linux shell
- Git
- Python
- Adobe lightroom

## Experience

`2016-2017`
__Head of Internal Department__ | UKAFO (ITS Photography Club) <br>
Supervised all internal events such as recuitment new member, welcome party, mentoring program, photography workshop, and photoshoot (weekly event).

`2016-2017`
__Teaching Assistant__ | PAPSI ITS <br>
Assisted student who has trouble following the lecture or has an error while the lecture ongoing.

`2015-2016`
__Staff Internal Department__ | UKAFO (ITS Photography Club) <br>
Organized one of internal events: plotting for mentoring program between staff and new member.

## Projects

`2020`
__Instasaver__ <br>
Combination of instaloader python module and streamlit. Live demo: <a href="https://intip.in/instasaver">intip.in/instasaver</a> (streamlit sharing) and <a href="https://instasaver.js.org/">instasaver.js.org</a> (heroku).

`2019-2020`
__Pavement Distress Detector__ <br>
Implementation of Single Shot Detector (SSD) to detect pavement distresses.
The best result was 75.45% accuracy on video with less small-sized pavement distresses.

<!-- ## Areas of expertise

* Machine learning
* Data visualisation
* Computer vision -->

## Links

<!-- fa are fontawesome, ai are academicons -->
- <i class="fa fa-envelope"></i> <a href="mailto:diawan@pm.me">diawan@pm.me</a><br />
- <i class="fa fa-github"></i> <a href="http://github.com/bruhtus">bruhtus</a><br />
- <i class="fa fa-twitter"></i> <a href="http://twitter.com/diawanchris">diawanchris</a><br />
- <i class="fa fa-linkedin"></i> <a href="https://www.linkedin.com/in/bruhtus/">bruhtus</a>

<!-- ### Footer

Last updated: May 2013 -->
